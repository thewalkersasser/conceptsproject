
# Parser class
class Parser:
    # should take in tokens and parse them
    def __init__(self, tokens):
        self.tokens = tokens
        self.position = 0 # list of tokens, start at index 0, which is first line of code
        self.current_token = self.tokens[self.position]
        self.next()

    def next(self):
        self.position += 1
        if self.position < len(self.tokens):
            self.current_token = self.tokens[self.position]
        return self.current_token

    # methods for creating sentences from the tokens

    # factor as we defined it
    def factor(self):
        if (self.current_token in ('INT', 'FLOAT', 'BOOL_LITERAL')):
            self.next()
            return Number(self.current_token)

    # expression as we defined it
    def expr(self):
        return self.binaryOperation(self.factor(), ('PLUS', 'MINUS'))
    def oper(self):
        return self.binaryOperation(self.factor(), ('DIV', 'MUl'))
    def parse(self):
        result = self.oper()
        return result


    def binaryOperation(self, function, operation):
        left = self.factor()
        while self.current_token in operation:
            operation = self.current_token
            self.next()
            right = self.factor()
        left = Operator(left, operation, right)
        return left


# Number class/node
class Number:
    def __init__(self, token):
        self.token = token

    def __repr__(self):
        return str(self.token)

# Operator class/node
class Operator:
    # needs to have a connection to left node, an operator (+ - / * or not...) and a right node
    def __init__(self, left_node, operator, right_node):
        self.left_node = left_node
        self.operator = operator
        self.right_node = right_node


    def __repr__(self):
        return f'({self.left_node}, {self.operator}, {self.right_node})'
#