# /*
#  * Class:       CS 4308 Section 1
#  * Term:        Fall 2020
#  * Name:        Walker Sasser
#  * Instructor:   Deepa Muralidhar
#  * Project:  Deliverable 1 Scanner - Python
#  */
#

# Let's begin

# Literals
INT = 'INT'
FLOAT = 'FLOAT'
BOOLEAN = 'BOOL_LITERAL'

# Math ops
PLUS = 'PLUS'
MINUS = 'MINUS'
MUL = 'MUL'
DIV = 'DIV'
NOT_EQ = 'NOT_EQ'

# Other tokens
ASSIGN = 'ASSIGN'
LPAREN = 'LPAREN'
RPAREN = 'RPAREN'
IS_EQUAL = 'IS_EQUAL'
NOT = 'NOT'
AND = 'AND'
OR = 'OR'
IF = 'IF'
ELSE = 'ELSE'
END = 'END'
WHILE = 'WHILE'
KEYWORD = 'KEYWORD'
VAR_ID = 'VAR_ID'

# not sure if we need this but why not
SEMI = 'SEMI'  #

# some constants
NUMBERS = '0123456789'
CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'


# Error Class
class Error:
    def __init__(self, pointer_start, pointer_end, error_name, description):
        self.pointer_start = pointer_start  # starting position of error
        self.pointer_end = pointer_end  # end position of error
        self.name = error_name  # name of the error
        self.description = description  # description of the error

    # print the error
    def to_string(self):
        result = 'Error Name: {}: {}'.format(self.name, self.description)
        result = result + 'FILE: {}, line {}'.format(self.pointer_start.filename, self.pointer_start.line + 1)
        return result


# subclass of Error, this will use the built in python IllegalCharError to display an error with input to the scanner
class IllegalCharError(Error):
    def __init__(self, position_start, position_end, description):
        super().__init__(position_start, position_end, 'ILLEGAL CHARACTER', description)


# the pointer class is meant to be a 'pointer' to the position of the input string the lexer object is scanning
class Pointer:
    def __init__(self, index, line, column, filename, file_content):
        self.index = index  # file index
        self.line = line  # line number
        self.column = column  # column number
        self.filename = filename  # filename
        self.file_content = file_content  # string in the file

    # advance to the next character
    def next(self, current_character):
        self.index = self.index + 1
        self.column = self.column + 1

        # if it's the end of a line, increment line and cloumns
        if current_character == '\n':
            self.line = self.line + 1
            self.column = self.column + 1

        return self

    # sometimes we may need a copy of a position (so we can keep moving but remember a location)
    def duplicate(self):
        return Pointer(self.index, self.line, self.column, self.filename, self.file_content)


# Token Class - it's pretty simple, just kinda a wrapper around tokens.
class Token:
    def __init__(self, type_, value=None):
        self.type = type_  # type of Token (keyword, int, float, etc)
        self.value = value  # value

    # a fancy way to print out the details of an object
    def __repr__(self):
        if self.value: return f'{self.type}:{self.value}'
        return f'{self.type}'


# Lexer Class - main class, as this is where most of the magic happens. Scanning.
class Lexer:
    def __init__(self, filename, text):
        self.text = text  # text in the file
        self.filename = filename  # filename
        self.position = Pointer(-1, 0, -1, self.filename,
                                self.text)  # Pointer object, which points to the current position of the scanner
        self.current_character = None  # initial current_character is None
        self.current_index = -1
        self.next_character = None
        self.next()  # this advances the position to character 1 of n characters in text

    # advance position of scanner head
    def next(self):
        self.position = self.position.next(self.current_character)
        self.current_index = self.current_index + 1
        # if position is not out of range (end of text)
        if self.position.index < len(self.text):  # if it's more than len(self.text), we've reached the end of the file
            self.current_character = self.text[self.position.index]
        else:
            self.current_character = None

    def next_char(self):
        # if position is not out of range (end of text)
        if (self.current_index + 1) < len(self.text):  # if it's more than len(self.text), we've reached the end of the file
            self.next_character = self.text[self.current_index + 1]
        else:
            self.next_character = None

# generate a list of tokens
    def generate_tokens(self):

        # list of tokens
        tokens = []

        token_dict = {
            '(': Token(LPAREN),
            ')': Token(RPAREN),
            '*': Token(MUL),
            '/': Token(DIV),
            '-': Token(MINUS),
            '+': Token(PLUS),
            ';': Token(SEMI)
        }

        # while not at the end of the file text
        while self.current_character != None:

            # basically, if symbol, add token of type symbol
            # if number or non-symbol character, call helper methods to decipher
            if self.current_character in ' ' or self.current_character in '\t':
                self.next()
            elif (self.current_character in NUMBERS):
                tokens.append(self.make_number())  # number can me multiple digits
            elif (self.current_character in CHARACTERS):
                tokens.append(self.make_keyword())  # keyword can me multiple characters
            elif (self.current_character == '!'):  # != symbol
                self.next_char()
                if (self.next_character == '='):
                    tokens.append(Token(IS_EQUAL))
                self.next()
            elif (self.current_character == '='):  # == symbol
                self.next_char()
                if (self.next_character == '='):
                    tokens.append(Token(IS_EQUAL))
                else:
                    tokens.append(Token(ASSIGN))
                self.next()
            elif (self.current_character in token_dict.keys()):
                # for single character tokens, we use the token_dict to
                # avoid writiing a bunch of if/elif statements for them.
                tokens.append(token_dict[self.current_character])
                self.next()
            else:
                # if all else fails, then we've hit a character that is a no no
                position_start = self.position.duplicate()  # save the position
                character = self.current_character
                self.next()  # advance
                # return an empty token list along with an IllegalCharacter error
                return [], IllegalCharError(position_start, self.position, character)

        return tokens, None

        # this method is called if the self.current character is not a symbol

    # but not a number/digit either
    # it generates keywords, and boolean literal tokens (also var id's)
    # we may need to come up with a more comprehensive name
    def make_keyword(self):
        keyword = ''

        while self.current_character != None and self.current_character in CHARACTERS:
            keyword = keyword + self.current_character  # build out our mystery 'keyword'
            self.next()

            # this works just like the elif structure in generate tokens,
        # except it's for multicharacter elements

        if len(keyword) == 1:
            return Token(VAR_ID, keyword)

        keyword_dict = {
            'and': Token(KEYWORD, 'AND'),
            'or': Token(KEYWORD, 'OR'),
            'end': Token(KEYWORD, 'END'),
            'not': Token(KEYWORD, 'NOT'),
            'if': Token(KEYWORD, 'IF'),
            'else': Token(KEYWORD, 'ELSE'),
            'true': Token(BOOLEAN, 'TRUE'),
            'false': Token(BOOLEAN, 'FALSE'),
            'while': Token(KEYWORD, 'WHILE'),
        }

        # if a something comes back then it's in the keyword dict
        if (keyword_dict[keyword.lower()] != None):
            # return that token
            return keyword_dict[keyword.lower()]
        else:
            # return the whole keyword
            return keyword

    # this is make_keyword but for numbers
    # because numbers can be multidigit
    def make_number(self):
        number = ''
        decimal_counter = 0

        while self.current_character != None and self.current_character in NUMBERS + '.':
            if self.current_character == '.':  # if there is a decimal in the number
                if decimal_counter == 1:  # and there's not more than 1 decimal
                    break
                decimal_counter = decimal_counter + 1
                number = number + '.'  # add the decimal to the number
            else:
                number = number + self.current_character
            self.next()

        if decimal_counter == 0:
            return Token(INT, int(number))  # no decimals, so create int token and pass in the number
        else:
            return Token(FLOAT, float(number))


# make a lexer object and start scanning
def execute(filename, text):
    # we chose to have our scanner build a list of list
    # each entry is a line, and within each entry is a list of tokens
    # found on that line
    # this is subject to change
    line_list = []

    with open(filename) as f:
        text = f.read().split('\n')
        # for each line in the text of the file
        for line in text:
            lexer_agent = Lexer(filename, line)  # create a lexer object
            tokens, error = lexer_agent.generate_tokens()  # generate the tokens for that line
            line_list.append(
                tokens)  # this line is not currently doing anything as we are not passing this data on to a parser yet
            if (len(tokens) >= 1):
                if (error == None):
                    print(tokens)  # if no error, print the tokens!
            elif (error != None):
                print(tokens, error)  # if error, print the tokens of that line and error(s)
    return line_list,error


# Parser class
class Parser:
    # should take in tokens and parse them
    def __init__(self, tokens):
        self.tokens = tokens
        self.position = -1 # list of tokens, start at index 0, which is first line of code
        self.current_token = self.tokens[self.position]
        self.next()

    def next(self):
        self.position += 1
        if self.position < len(self.tokens):
            self.current_token = self.tokens[self.position]
        return self.current_token
    def parse(self):
        res = ''
        if(self.current_token.type in VAR_ID):
            if(self.tokens[self.position + 1].type in ASSIGN):
                curr_token = self.current_token
                self.next()
                next_token =  self.current_token
                self.next()
                res = self.assignmnet(curr_token, next_token)
        else:
            res = self.expression()
        return res
    def assignmnet(self, current_Token, next_Token):
        arithmetic = self.expression()
        return f'( {current_Token} {next_Token} {arithmetic}'
    # methods for creating sentences from the tokens

    # factor as we defined it
    def expression(self):
        return self.binaryOperation(self.term, (PLUS, MINUS))


    def term(self):
        return self.binaryOperation(self.factor, (MUL, DIV))


    def factor(self):
        token = self.current_token
        if (token.type in (INT, FLOAT, BOOLEAN)):
            self.next()
            return Number(token)

    # expression as we defined it
    # def expr(self):
    #     return self.binaryOperation(self.factor(), ('PLUS', 'MINUS'))
    # def oper(self):
    #     return self.binaryOperation(self.factor(), ('DIV', 'MUl'))
    # def parse(self):
    #     result = self.oper()
    #     return result
    #
    #
    def binaryOperation(self, function, ops):
        left = function()
        while self.current_token.type in ops:
            operation = self.current_token
            self.next()
            right = function()
            left = Operator(left, operation, right)
        return left


# Number class/node
class Number:
    def __init__(self, token):
        self.token = token

    def __repr__(self):
        return f'{(self.token)}'

# Operator class/node
class Operator:
    # needs to have a connection to left node, an operator (+ - / * or not...) and a right node
    def __init__(self, left_node, operator, right_node):
        self.left_node = left_node
        self.operator = operator
        self.right_node = right_node


    def __repr__(self):
        return f'({self.left_node}, {self.operator}, {self.right_node})'
#



def main():
    filename = input('ADA > ')
    while filename != 'exit':
        text = ''
        result, error = execute(filename, text)
        result = [ele for ele in result if ele != []]
        print("\n\nRESULT GOING TO PARSER: ", result)
        # if error != None:
        #     print(error.to_string())
        # else:
        #     print(result)



        filename = input('ADA > ')  # ask the user for another file name, or enter exit to end the scanner execution

    for i in result:
        parser = Parser(i)
        ast = parser.parse()
        print(ast)

main()  # call the main method to start the scanner


